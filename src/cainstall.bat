@ECHO OFF
REM Allow for variables to be set and used in IF statments
setlocal EnableDelayedExpansion
REM Set variables
set localdir=c:\batch-install
set remotedir=\\iso-drobo\Public\ISO Images\ISO\
cd c:\
rmdir /s /q "%localdir%"
mkdir "%localdir%"
cd "%localdir%"

CLS
:MENU
CLS
ECHO.
ECHO ==================================
ECHO WHICH PROGRAM WOULD YOU LIKE TO INSTALL?
ECHO ==================================
ECHO.
ECHO 1 - Chief Architect
ECHO 2 - Home Designer
ECHO 3 - Clean Up Desktop Icons
ECHO X - Exit
ECHO.

set input=
set /p input="->"

if /i {%input%} == {1} (goto :CA)
if /i {%input%} == {2} (goto :HD)
if /i {%input%} == {3} (goto :CLEAN)
if /i {%input%} == {X} (goto :EXIT)

:CA
CLS
ECHO ==================================
ECHO WHICH CHIEF ARCHITECT VERSION WOULD YOU LIKE TO INSTALL?
ECHO ==================================
ECHO.
ECHO 1. Chief Architect X10
ECHO 2. Chief Architect X9
ECHO 3. Chief Architect X8
ECHO 4. Chief Architect X7
ECHO 5. Chief Architect X6
ECHO 6. Chief Architect X5
ECHO 7. Chief Architect X4
ECHO 8. Chief Architect X3
ECHO 0. Return to Main Menu
ECHO X. EXIT
ECHO.

set programver=
set programfam=Chief Architect
set input=
set /p input="->"

if /i {%input%} == {1} set version=X10& set build=20
if /i {%input%} == {2} set version=X9& set build=19
if /i {%input%} == {3} set version=X8& set build=18
if /i {%input%} == {4} set version=X7& set build=17
if /i {%input%} == {5} set version=X6& set build=16
if /i {%input%} == {6} set version=X5& set build=15
if /i {%input%} == {7} set version=X4& set build=14
if /i {%input%} == {8} set version=X3& set build=13
if /i {%input%} == {0} goto :MENU
if /i {%input%} == {X} goto :EXIT

goto :KEY

:HD
CLS
ECHO ==================================
ECHO WHICH HOME DESIGNER VERSION WOULD YOU LIKE TO INSTALL?
ECHO ==================================
ECHO.
ECHO 1. Home Designer 2019
ECHO 2. Home Designer 2018
ECHO 3. Home Designer 2017
ECHO 4. Home Designer 2016
ECHO 5. Home Designer 2015
ECHO 6. Home Designer 2014
ECHO 7. Home Designer 2012
ECHO 8. Home Designer 10
ECHO 0. Return to Main Menu
ECHO X. EXIT
ECHO.

set programfam=Home Designer
set input=
set /p input="->"

if /i {%input%} == {1} set version=2019& set build=20
if /i {%input%} == {2} set version=2018& set build=19
if /i {%input%} == {3} set version=2017& set build=18
if /i {%input%} == {4} set version=2016& set build=17
if /i {%input%} == {5} set version=2015& set build=16
if /i {%input%} == {6} set version=2014& set build=15
if /i {%input%} == {7} set version=2012& set build=11
if /i {%input%} == {8} set version=10& set build=10
if /i {%input%} == {0} goto :MENU
if /i {%input%} == {X} goto :EXIT

goto :KEY

:KEY
CLS
ECHO ==================================
ECHO Enter Product Key
ECHO ==================================

set input=
set /p input="->"

set productkey=%input%

goto :COPYPATH

:COPYPATH
set remotepath=%remotedir%%programfam% %version%
IF "%programfam%"=="Chief Architect" (
    IF "%programver%"=="" (
        SET programver=Premier
    )
)
IF "%programfam%"=="Home Designer" (
    IF "%programver%"=="" (
        SET programver=Pro
    )
)
for /f "delims=|" %%i in ('dir /b/od/t:c "%remotepath%\%programfam% !programver!\%programfam% !programver! (*)"') do set newestfile=%%i
set downloadpath=%remotepath%\%programfam% !programver!\!newestfile!
goto :DOWNLOAD
PAUSE

:DOWNLOAD
robocopy "%downloadpath%" "%localdir%" *.msi /s
IF "%programfam%"=="Chief Architect" (
    IF "%programver%"=="Premier" (
        SET programver=Interiors
        goto :COPYPATH
    )
    IF "%programver%"=="Interiors" (
        IF %build% LEQ 16 (
            SET programver=Lite
            goto :COPYPATH
        )
    )
    GOTO :INSTALL
)
IF "%programfam%"=="Home Designer" (
    IF "%programver%"=="Pro" (
        IF %build% EQU 20 (
            SET programver=Arch
            goto :COPYPATH
        )
        SET programver=Architectural
        goto :COPYPATH
    )
    IF "%programver%"=="Architectural" (
        SET programver=Suite
        goto :COPYPATH
    )
    IF "%programver%"=="Arch" (
        SET programver=Suite
        goto :COPYPATH
    )
    IF "%programver%"=="Suite" (
        SET programver=Essentials
        goto :COPYPATH
    )
    IF "%programver%"=="Essentials" (
        SET programver=Interiors
        goto :COPYPATH
    )
    IF "%programver%"=="Interiors" (
        IF %build% LEQ 11 (
            SET programver=Landscape and Deck
            goto :COPYPATH
        )
    )
    GOTO :INSTALL
)

:INSTALL
CLS
ECHO Installing downloaded versions
pushd "%localdir%"
for %%f in (*.msi) do (
    IF "%productkey%"=="" (
        msieXec /i %%f /passive
    ) ELSE (
        msieXec /i %%f PRODUCTKEY=%productkey% ALLUSERS=1 /passive
    )
)
IF "%programfam%"=="Chief Architect" (
    GOTO :CA
)
GOTO :HD

:CLEAN
CLS
mkdir "%userprofile\Documents\Chief Program Shortcuts\"
cd "%userprofile\Documents\Chief Program Shortcuts\" 
mkdir "Chief Architect Premier"
mkdir "Chief Architect Interiors"
mkdir "Chief Architect Lite"
mkdir "Home Designer Pro"
mkdir "Home Designer Architectural"
mkdir "Home Designer Landscape and Deck"
mkdir "Home Designer Suite"
mkdir "Home Designer Interiors"
mkdir "Home Designer Essentials"
cd C:\users\public\desktop
move /Y "Chief Architect Premier *.lnk" "%userprofile%\Documents\Chief Program Shortcuts\Chief Architect Premier\" > nul
move /Y "Chief Architect Interiors *.lnk" "%userprofile%\Documents\Chief Program Shortcuts\Chief Architect Interiors\" > nul
move /Y "Chief Architect Lite *.lnk" "%userprofile%\Documents\Chief Program Shortcuts\Chief Architect Lite\" > nul
move /Y "Home Designer Pro *.lnk" "%userprofile%\Documents\Chief Program Shortcuts\Home Designer Pro\"> nul
move /Y "Home Designer Architectural *.lnk" "%userprofile%\Documents\Chief Program Shortcuts\Home Designer Architectural\"> nul
move /Y "Home Designer Landscape *.lnk" "%userprofile%\Documents\Chief Program Shortcuts\Home Designer Landscape and Deck\"> nul
move /Y "Home Designer Interiors *.lnk" "%userprofile%\Documents\Chief Program Shortcuts\Home Designer Interiors\"> nul
move /Y "Home Designer Suite *.lnk" "%userprofile%\Documents\Chief Program Shortcuts\Home Designer Suite\"> nul
move /Y "Home Designer Essentials *.lnk" "%userprofile%\Documents\Chief Program Shortcuts\Home Designer Essentials\"> nul
rundll32 user32.dll,UpdatePerUserSystemParameters

GOTO :MENU

:EXIT
cd c:\
rmdir /s /q "%localdir%"