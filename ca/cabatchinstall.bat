@echo off
title Chief Architect Batch Installer

cd c:\
rmdir /s /q "c:\batch-install"
mkdir "c:\batch-install"
cd "c:\batch-install"

:begin
cls
echo ===========================================
echo *NOTE: This script will only work on the
echo  ART local network.
echo ============ MAIN PROGRAMS ==================
echo What would you like to do?
echo {1: Install all X2 products} {2: Install all X3 products}
echo {3: Install all X4 products} {4: Install all X5 products}
echo {5: Install all X6 products} {6: Install all X7 products}
echo {7: Install all X8 products} {8: Install all X9 products}
echo {9: Install all X10 products}
echo ============ OTHER OPTIONS ==================
echo {A: All of the above}
echo {B: Specify build number}
echo {C: Specify build number - Premier only}
echo ===========================================
echo {Q: Quit}

set input=
set /p input="->"

if /i {%input%} == {1}  (goto :1)
if /i {%input%} == {2}  (goto :2)
if /i {%input%} == {3}  (goto :3)
if /i {%input%} == {4}  (goto :4)
if /i {%input%} == {5}  (goto :5)
if /i {%input%} == {6}  (goto :6)
if /i {%input%} == {7}  (goto :7)
if /i {%input%} == {8}  (goto :8)
if /i {%input%} == {9}  (goto :9)
if /i {%input%} == {A}  (goto :A)
if /i {%input%} == {B}  (goto :B)
if /i {%input%} == {C}  (goto :C)
if /i {%input%} == {Q}  (goto :Q)
goto :begin

:1
robocopy "\\uranium\chiefgeneral\techgeneral\misc\batch install\X2" "c:\batch-install" *.* /s
pushd "c:\batch-install"
for %%f in (*.msi) do msieXec /i "%%f" /passive
cls
goto :begin

:2
robocopy "\\uranium\chiefgeneral\techgeneral\misc\batch install\X3" "c:\batch-install" *.msi /s
pushd "c:\batch-install"
for %%f in (*.msi) do msieXec /i %%f /passive
cls
goto :begin

:3
robocopy "\\uranium\chiefgeneral\techgeneral\misc\batch install\X4" "c:\batch-install" *.msi /s
pushd "c:\batch-install"
for %%f in (*.msi) do msieXec /i %%f /passive
cls
goto :begin

:4
robocopy "\\uranium\chiefgeneral\techgeneral\misc\batch install\X5" "c:\batch-install" *.msi /s
pushd "c:\batch-install"
for %%f in (*.msi) do msieXec /i %%f /passive
cls
goto :begin

:5
robocopy "\\uranium\chiefgeneral\techgeneral\misc\batch install\X6" "c:\batch-install" *.msi /s
pushd "c:\batch-install"
for %%f in (*.msi) do msieXec /i %%f /passive
cls
goto :begin

:6
robocopy "\\uranium\chiefgeneral\techgeneral\misc\batch install\X7" "c:\batch-install" *.msi /s
pushd "c:\batch-install"
for %%f in (*.msi) do msieXec /i %%f /passive
cls
goto :begin

:7
robocopy "\\uranium\chiefgeneral\techgeneral\misc\batch install\X8" "c:\batch-install" *.msi /s
pushd "c:\batch-install"
for %%f in (*.msi) do msieXec /i %%f /passive
cls
goto :begin

:8
robocopy "\\uranium\chiefgeneral\techgeneral\misc\batch install\X9" "c:\batch-install" *.msi /s
pushd "c:\batch-install"
for %%f in (*.msi) do msieXec /i %%f /passive
cls
goto :begin

:9
robocopy "\\uranium\chiefgeneral\techgeneral\misc\batch install\X10" "c:\batch-install" *.msi /s
pushd "c:\batch-install"
for %%f in (*.msi) do msieXec /i %%f /passive
cls
goto :begin

:A
robocopy "\\uranium\chiefgeneral\techgeneral\misc\batch install\X2" "c:\batch-install" *.* /s
robocopy "\\uranium\chiefgeneral\techgeneral\misc\batch install\X3" "c:\batch-install" *.msi /s
robocopy "\\uranium\chiefgeneral\techgeneral\misc\batch install\X4" "c:\batch-install" *.msi /s
robocopy "\\uranium\chiefgeneral\techgeneral\misc\batch install\X5" "c:\batch-install" *.msi /s
robocopy "\\uranium\chiefgeneral\techgeneral\misc\batch install\X6" "c:\batch-install" *.msi /s
robocopy "\\uranium\chiefgeneral\techgeneral\misc\batch install\X7" "c:\batch-install" *.msi /s
robocopy "\\uranium\chiefgeneral\techgeneral\misc\batch install\X8" "c:\batch-install" *.msi /s
robocopy "\\uranium\chiefgeneral\techgeneral\misc\batch install\X9" "c:\batch-install" *.msi /s
robocopy "\\uranium\chiefgeneral\techgeneral\misc\batch install\X10" "c:\batch-install" *.msi /s
pushd "c:\batch-install"
for %%f in (*.msi) do msieXec /i %%f /passive
cls
goto :begin

:B
set build=
set /p build="Enter the build number->"
set version=
set /p version="Enter product version(i.e. X6, X7, etc.)->"
for /f "delims=|" %%i in ('dir "\\iso-drobo\nightlybuilds\releases\chief architect premier (%build%*)" /b /o:d') do set newestfile=%%i
robocopy "\\iso-drobo\nightlybuilds\releases\%newestfile%" "c:\batch-install" *.msi /s
for /f "delims=|" %%i in ('dir "\\iso-drobo\nightlybuilds\releases\chief architect interiors (%build%*)" /b /o:d') do set newestfile=%%i
robocopy "\\iso-drobo\nightlybuilds\releases\%newestfile%" "c:\batch-install" *.msi /s
pushd "c:\batch-install"
for %%f in (*.msi) do msieXec /i %%f /passive
set icon=
set /p icon="would you like to turn the icon green?(y/n)->"
if /i {%icon%} == {y} (goto :copyicon)
if /i {%icon%} == {n} (goto :begin)
cls
goto :begin

:C
set build=
echo Enter the build you want to find. This can be part or all of a build number.(i.e. 19 or 19.1 or 19.1.0.47 are all valid)
set /p build="build number->"
set version=
set /p version="Enter product version(i.e. X6, X7, etc.)->"
for /f "delims=|" %%i in ('dir "\\iso-drobo\nightlybuilds\releases\chief architect premier (%build%*)" /b /o:d') do set newestfile=%%i
robocopy "\\iso-drobo\nightlybuilds\releases\%newestfile%" "c:\batch-install" *.msi /s
pushd "c:\batch-install"
for %%f in (*.msi) do msieXec /i %%f /passive
set icon=
set /p icon="would you like to turn the icon green? (Y)es / (N)o->"
if /i {%icon%} == {y} (goto :copyicon)
if /i {%icon%} == {n} (goto :begin)
cls
goto :begin

:copyicon
copy /y "\\uranium\ChiefGeneral\TechGeneral\Misc\Batch Install\ProductIconCAFull256_64.png" "C:\Program Files\Chief Architect\Chief Architect Premier %version% (64 bit)\resources\graphics\ProductIconCAFull256_64.png"
echo Icon successfully updated
pause
cls
goto :begin

:quit
cls
goto :begin

:Q
cd c:\
rmdir /s /q "c:\batch-install"