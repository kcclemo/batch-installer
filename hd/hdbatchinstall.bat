@ECHO off
TITLE Home Designer Batch Installer
MODE con cols=120 lines=30 > nul

CD C:\
RMDIR /S /Q "C:\Batch-Install"
MKDIR "C:\Batch-Install"
CD "C:\Batch-Install"

:Begin
CLS
ECHO ===========================================
ECHO *Note: This script will only work on the
ECHO  ART Local Network.
ECHO ============Main Programs==================
ECHO What would you like to do?
ECHO {1: Install All v8 Products} {2: Install All v9 Products}
ECHO {3: Install All v10 Products} {4: Install All 2012 Products}
ECHO {5: Install All 2014 Products} {6: Install All 2015 Products}
ECHO {7: Install All 2016 Products} {8: Install All 2017 Products}
echo {9: Install All 2018 Products} {10: Install All 2019 Products}
ECHO ============Other Options==================
echo {A: All of the above}
echo {B: Enter a specific build number}
ECHO {C: Cancel}
ECHO ===========================================
SET input=
SET /P input="->"

IF /i {%input%} == {1} (goto :1)
IF /i {%input%} == {2} (goto :2)
IF /i {%input%} == {3} (goto :3)
IF /i {%input%} == {4} (goto :4)
IF /i {%input%} == {5} (goto :5)
IF /i {%input%} == {6} (goto :6)
IF /i {%input%} == {7} (goto :7)
IF /i {%input%} == {8} (goto :8)
IF /i {%input%} == {9} (goto :9)
IF /i {%input%} == {10} (goto :10)
IF /i {%input%} == {A} (goto :A)
IF /i {%input%} == {B} (goto :B)
IF /i {%input%} == {C} (goto :C)
GOTO :Begin

:1
ROBOCOPY "\\uranium\ChiefGeneral\TechGeneral\Misc\Batch Install\HDv8" "C:\Batch-Install" *.* /s
FOR %%f IN (*.msi) DO MSIEXEC /i "%%f" /passive
CLS
GOTO :Begin

:2
ROBOCOPY "\\uranium\ChiefGeneral\TechGeneral\Misc\Batch Install\HDv9" "C:\Batch-Install" *.* /s
FOR %%f IN (*.msi) DO MSIEXEC /i "%%f" /passive
CLS
GOTO :Begin

:3
ROBOCOPY "\\uranium\chiefgeneral\TechGeneral\Misc\Batch Install\HDv10" "C:\Batch-Install" *.msi /s
FOR %%f IN (*.msi) DO MSIEXEC /i %%f /passive
CLS
GOTO :Begin

:4
ROBOCOPY "\\uranium\chiefgeneral\TechGeneral\Misc\Batch Install\HD2012" "C:\Batch-Install" *.msi /s
FOR %%f IN (*.msi) DO MSIEXEC /i %%f /passive
CLS
GOTO :Begin

:5
ROBOCOPY "\\uranium\chiefgeneral\TechGeneral\Misc\Batch Install\HD2014" "C:\Batch-Install" *.msi /s
FOR %%f IN (*.msi) DO MSIEXEC /i %%f /passive
CLS
GOTO :Begin

:6
ROBOCOPY "\\uranium\chiefgeneral\TechGeneral\Misc\Batch Install\HD2015" "C:\Batch-Install" *.msi /s
FOR %%f IN (*.msi) DO MSIEXEC /i %%f /passive
CLS
GOTO :Begin

:7
ROBOCOPY "\\uranium\chiefgeneral\TechGeneral\Misc\Batch Install\HD2016" "C:\Batch-Install" *.msi /s
FOR %%f IN (*.msi) DO MSIEXEC /i %%f /passive
CLS
GOTO :Begin

:8
ROBOCOPY "\\uranium\chiefgeneral\TechGeneral\Misc\Batch Install\HD2017" "C:\Batch-Install" *.msi /s
FOR %%f IN (*.msi) DO MSIEXEC /i %%f /passive
CLS
GOTO :Begin

:9
ROBOCOPY "\\uranium\chiefgeneral\TechGeneral\Misc\Batch Install\HD2018" "C:\Batch-Install" *.msi /s
FOR %%f IN (*.msi) DO MSIEXEC /i %%f /passive
CLS
GOTO :Begin

:10
ROBOCOPY "\\uranium\chiefgeneral\TechGeneral\Misc\Batch Install\HD2019" "C:\Batch-Install" *.msi /s
FOR %%f IN (*.msi) DO MSIEXEC /i %%f /passive
CLS
GOTO :Begin

:A
ROBOCOPY "\\uranium\chiefgeneral\TechGeneral\Misc\Batch Install\HDv10" "C:\Batch-Install" *.msi /s
ROBOCOPY "\\uranium\chiefgeneral\TechGeneral\Misc\Batch Install\HD2012" "C:\Batch-Install" *.msi /s
ROBOCOPY "\\uranium\chiefgeneral\TechGeneral\Misc\Batch Install\HD2014" "C:\Batch-Install" *.msi /s
ROBOCOPY "\\uranium\chiefgeneral\TechGeneral\Misc\Batch Install\HD2015" "C:\Batch-Install" *.msi /s
ROBOCOPY "\\uranium\chiefgeneral\TechGeneral\Misc\Batch Install\HD2016" "C:\Batch-Install" *.msi /s
ROBOCOPY "\\uranium\chiefgeneral\TechGeneral\Misc\Batch Install\HD2017" "C:\Batch-Install" *.msi /s
ROBOCOPY "\\uranium\chiefgeneral\TechGeneral\Misc\Batch Install\HD2018" "C:\Batch-Install" *.msi /s
ROBOCOPY "\\uranium\chiefgeneral\TechGeneral\Misc\Batch Install\HD2019" "C:\Batch-Install" *.msi /s
FOR %%f IN (*.msi) DO MSIEXEC /i %%f /passive
CLS
GOTO :Begin

:B
SET build=
SET /P build="Enter the build number->"

FOR /F "delims=|" %%I IN ('dir "\\iso-drobo\NightlyBuilds\releases\Home Designer Pro (%build%*)" /B /O:D') DO SET NewestFile=%%I
ROBOCOPY "\\iso-drobo\NightlyBuilds\releases\%NewestFile%" "C:\Batch-Install" *.msi /s
FOR /F "delims=|" %%I IN ('dir "\\iso-drobo\NightlyBuilds\releases\Home Designer Architectural (%build%*)" /B /O:D') DO SET NewestFile=%%I
ROBOCOPY "\\iso-drobo\NightlyBuilds\releases\%NewestFile%" "C:\Batch-Install" *.msi /s
FOR /F "delims=|" %%I IN ('dir "\\iso-drobo\NightlyBuilds\releases\Home Designer Suite (%build%*)" /B /O:D') DO SET NewestFile=%%I
ROBOCOPY "\\iso-drobo\NightlyBuilds\releases\%NewestFile%" "C:\Batch-Install" *.msi /s
FOR /F "delims=|" %%I IN ('dir "\\iso-drobo\NightlyBuilds\releases\Home Designer Interiors (%build%*)" /B /O:D') DO SET NewestFile=%%I
ROBOCOPY "\\iso-drobo\NightlyBuilds\releases\%NewestFile%" "C:\Batch-Install" *.msi /s
FOR /F "delims=|" %%I IN ('dir "\\iso-drobo\NightlyBuilds\releases\Home Designer Essentials (%build%*)" /B /O:D') DO SET NewestFile=%%I
ROBOCOPY "\\iso-drobo\NightlyBuilds\releases\%NewestFile%" "C:\Batch-Install" *.msi /s
FOR %%f IN (*.msi) DO MSIEXEC /i %%f /passive
CLS
GOTO :Begin

:Quit
CLS
GOTO :Begin

:C
CD C:\
RMDIR /S /Q "C:\Batch-Install"